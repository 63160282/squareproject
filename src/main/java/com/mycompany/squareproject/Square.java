/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.squareproject;

/**
 *
 * @author User
 */
public class Square {
    double side;
    public Square(double side){
        this.side=side;
    }
    public double SquareArea(){
        return side*side;
    }
    public double getR(){
        return side;
    }
    public void setR(double side){
        if(side<=0) {
            System.out.println("Error Radius must more than Zero!!!");
            return;
        }
        this.side = side;
    }  
    
}
